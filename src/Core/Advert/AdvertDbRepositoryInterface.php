<?php
declare(strict_types=1);

namespace Spotahome\Core\Advert;

use Spotahome\Core\Advert\ValueObject\AdvertSort;

interface AdvertDbRepositoryInterface
{
    /**
     * @param AdvertSort $field
     * @return Advert[]
     */
    public function getAllSortedBy(AdvertSort $field): array;

    /**
     * @param Advert[] $adverts
     */
    public function bulkInsert(array $adverts): void;
}
