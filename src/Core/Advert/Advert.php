<?php
declare(strict_types=1);

namespace Spotahome\Core\Advert;

class Advert
{
    /** @var string */
    private $id;

    /** @var string */
    private $title;

    /** @var string */
    private $link;

    /** @var string */
    private $city;

    /** @var string */
    private $mainImage;

    public function __construct(string $id, string $title, string $link, string $city, string $mainImage)
    {
        $this->id = $id;
        $this->title = $title;
        $this->link = $link;
        $this->city = $city;
        $this->mainImage = $mainImage;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getLink(): string
    {
        return $this->link;
    }

    public function getCity(): string
    {
        return $this->city;
    }

    public function getMainImage(): string
    {
        return $this->mainImage;
    }
}
