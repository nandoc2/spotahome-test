<?php
declare(strict_types=1);

namespace Spotahome\Core\Advert;

use Spotahome\Core\Advert\ValueObject\AdvertSort;

class AdvertService
{
    /** @var AdvertDbRepositoryInterface */
    private $dbRepository;

    /** @var AdvertRemoteRepositoryInterface */
    private $remoteRepository;

    public function __construct(
        AdvertDbRepositoryInterface $dbRepository,
        AdvertRemoteRepositoryInterface $remoteRepository
    ) {
        $this->dbRepository = $dbRepository;
        $this->remoteRepository = $remoteRepository;
    }

    public function importAdverts(): void
    {
        $this->dbRepository->bulkInsert(
            $this->remoteRepository->fetchAll()
        );
    }

    /**
     * @param AdvertSort $field
     * @return Advert[]
     */
    public function getAllSortedBy(AdvertSort $field): array
    {
        return $this->dbRepository->getAllSortedBy($field);
    }
}
