<?php
declare(strict_types=1);

namespace Spotahome\Core\Advert\Exception;

class AdvertRemoteRequestException extends \Exception
{
}
