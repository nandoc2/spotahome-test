<?php
declare(strict_types=1);

namespace Spotahome\Core\Advert\ValueObject;

class AdvertSort
{
    private const SORT_BY = [
        'id',
        'title',
        'city',
    ];

    /** @var string */
    private $value;

    public function __construct(string $field)
    {
        if (!in_array($field, self::SORT_BY, true)) {
            throw new \InvalidArgumentException("Invalid sort value {$field}");
        }

        $this->value = $field;
    }

    public function getValue(): string
    {
        return $this->value;
    }
}
