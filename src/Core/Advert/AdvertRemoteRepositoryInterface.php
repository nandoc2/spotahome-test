<?php
declare(strict_types=1);

namespace Spotahome\Core\Advert;

use Spotahome\Core\Advert\Exception\AdvertRemoteRequestException;

interface AdvertRemoteRepositoryInterface
{
    /**
     * @return Advert[]
     * @throws AdvertRemoteRequestException
     */
    public function fetchAll(): array;
}
