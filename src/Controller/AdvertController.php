<?php

namespace Spotahome\Controller;

use Spotahome\Core\Advert\AdvertService;
use Spotahome\Core\Advert\ValueObject\AdvertSort;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\HeaderUtils;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;

class AdvertController extends AbstractController
{
    /** @var AdvertService */
    private $advertService;

    public function __construct(AdvertService $advertService)
    {
        $this->advertService = $advertService;
    }

    public function import(): Response
    {
        $this->advertService->importAdverts();

        return new JsonResponse(['status' => 'success']);
    }

    public function list(Request $request): Response
    {
        $sortFiled = new AdvertSort($request->query->get('sort', 'id'));

        return $this->render('advert/index.html.twig', [
            'adverts' => $this->advertService->getAllSortedBy($sortFiled),
            'sortField' => $sortFiled->getValue(),
        ]);
    }

    public function download(Request $request, SerializerInterface $serializer): Response
    {
        $sortFiled = new AdvertSort($request->query->get('sort', 'id'));

        $response = new Response($serializer->serialize($this->advertService->getAllSortedBy($sortFiled), 'json'));

        $response->headers->set(
            'Content-Disposition',
            HeaderUtils::makeDisposition(
            HeaderUtils::DISPOSITION_ATTACHMENT,
            'adverts.json'
            )
        );

        return $response;
    }
}
