<?php
declare(strict_types=1);

namespace Spotahome\Infrastructure\Remote;

use GuzzleHttp\Client;
use SimpleXMLElement;
use Spotahome\Core\Advert\Advert;
use Spotahome\Core\Advert\AdvertRemoteRepositoryInterface;
use Spotahome\Core\Advert\Exception\AdvertRemoteRequestException;

class AdvertRemoteRepository implements AdvertRemoteRepositoryInterface
{
    /** @var Client */
    private $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @inheritdoc
     */
    public function fetchAll(): array
    {
        try {
            $response = $this->client->get($this->client->getConfig('base_uri'));
            $xml = (array) (new SimpleXMLElement((string) $response->getBody()));

            return array_map(function (SimpleXMLElement $advert) {
                return new Advert(
                    (string) $advert->id,
                    (string) $advert->title,
                    (string) $advert->url,
                    (string) $advert->city,
                    (string) $advert->pictures->picture[0]->picture_url
                );
            }, $xml['ad']);
        } catch (\Throwable $e) {
            throw new AdvertRemoteRequestException("Could not retrieve adverts: {$e->getMessage()}", $e->getCode(), $e);
        }
    }
}
