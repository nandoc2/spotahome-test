<?php
declare(strict_types=1);

namespace Spotahome\Infrastructure\Database;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query;
use Spotahome\Core\Advert\Advert;
use Spotahome\Core\Advert\AdvertDbRepositoryInterface;
use Spotahome\Core\Advert\ValueObject\AdvertSort;

class AdvertDbRepository implements AdvertDbRepositoryInterface
{
    /** @var EntityManagerInterface */
    private $em;

    public function __construct(Registry $registry)
    {
        $this->em = $registry->getManager('advert');
    }

    /**
     * @inheritdoc
     */
    public function getAllSortedBy(AdvertSort $field): array
    {
        $qb = $this->em->createQueryBuilder();
        $qb
            ->select('a')
            ->from(Advert::class, 'a')
            ->orderBy('a.' . $field->getValue(), 'ASC');

        return $qb->getQuery()->setHint(Query::HINT_REFRESH, true)->getResult();
    }

    /**
     * @inheritdoc
     */
    public function bulkInsert(array $adverts): void
    {
        array_walk($adverts, function (Advert $advert) {
           $this->em->merge($advert);
        });

        $this->em->flush();
        $this->em->clear();
    }
}
