# Spotahome - Adverts

## Installation

Configure env vars
```text
cp .env.dist .env
```

Composer
```bash
composer install
```

Create DB
```text
bin/console doctrine:database:create
```

Run migrations
```text
bin/console doctrine:migrations:migrate
```

## Test

Create test DB
```bash
bin/console --env=test doctrine:database:create
bin/console --env=test doctrine:schema:update --force
```

Run tests and static analysis
```bash
php vendor/bin/phpunit
php vendor/bin/behat
composer phpstan
```

## Run

Fetch and import ads from external URL
```
POST /adverts/import
```

Display ads page
```text
GET /adverts
```

Download ads as JSON file
```text
GET /adverts/download
```

## TODO
- API docs
- Endpoints pagination
