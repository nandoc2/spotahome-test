Feature:
  Advert functional tests

  Scenario: It imports data
    Given the list of adverts is empty
    When I request import adverts
    Then the list of adverts is not empty

  Scenario: It downloads the adverts as json
    Given I request import adverts
    When I request to download the adverts
    Then I get a json file
