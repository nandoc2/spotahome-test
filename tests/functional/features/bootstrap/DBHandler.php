<?php
declare(strict_types=1);

namespace Spotahome\Tests\Functional\Features\Bootstrap;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\EntityManagerInterface;

class DBHandler
{
    /** @var EntityManagerInterface */
    private $advertEntityManager;

    public function __construct(Registry $registry)
    {
        $this->advertEntityManager = $registry->getManager('advert');
    }

    public function selectFrom(string $table): array
    {
        $conn = $this->advertEntityManager->getConnection();
        $stmt = $conn->executeQuery("SELECT * FROM {$table}");
        return $stmt->fetchAll();
    }

    public function truncateTable(string $table): void
    {
        $conn = $this->advertEntityManager->getConnection();
        $conn->executeQuery("DELETE FROM {$table}");
    }
}
