<?php

namespace Spotahome\Tests\Functional\Features\Bootstrap;

use Behat\Behat\Context\Context;
use Behat\Behat\Hook\Scope\AfterScenarioScope;
use PHPUnit\Framework\Assert;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * This context class contains the definitions of the steps used by the demo 
 * feature file. Learn how to get started with Behat and BDD on Behat's website.
 * 
 * @see http://behat.org/en/latest/quick_start.html
 */
class FeatureContext implements Context
{
    private const ADVERT_TABLE = 'advert';

    /**
     * @var KernelInterface
     */
    private $kernel;

    /** @var DBHandler */
    private $dbHandler;

    /**
     * @var Response|null
     */
    private $response;

    public function __construct(KernelInterface $kernel, DBHandler $dbHandler)
    {
        $this->kernel = $kernel;
        $this->dbHandler = $dbHandler;
    }

    /**
     * @AfterScenario @database
     */
    public function cleanDB(AfterScenarioScope $event)
    {
        // todo: refactor
        $this->dbHandler->truncateTable(self::ADVERT_TABLE);
    }

    /**
     * @Given the list of adverts is empty
     */
    public function theListOfAdvertsIsEmpty()
    {
        $this->dbHandler->truncateTable('advert');
        Assert::assertCount(0, $this->dbHandler->selectFrom(self::ADVERT_TABLE));
    }

    /**
     * @When I request import adverts
     */
    public function iRequestImportAdverts()
    {
        $this->response = $this->kernel->handle(Request::create('/adverts/import', 'POST'));
    }

    /**
     * @Then the list of adverts is not empty
     */
    public function theListOfAdvertsIsNotEmpty()
    {
        Assert::assertGreaterThan(0, $this->dbHandler->selectFrom(self::ADVERT_TABLE));
    }

    /**
     * @When I request to download the adverts
     */
    public function iRequestToDownloadTheAdverts()
    {
        $this->response = $this->kernel->handle(Request::create('/adverts/download', 'GET'));
    }

    /**
     * @Then /^I get a json file$/
     */
    public function iGetAJsonFile()
    {
        $jsonResponse = json_decode($this->response->getContent(), true);

        Assert::assertSame(200, $this->response->getStatusCode());
        Assert::assertGreaterThan(0, $jsonResponse);
        Assert::assertArrayHasKey('id', $jsonResponse[0]);
        Assert::assertArrayHasKey('title', $jsonResponse[0]);
    }
}
