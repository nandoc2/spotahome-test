<?php
declare(strict_types=1);

namespace Spotahome\Tests\Core\Advert\ValueObject;

use PHPUnit\Framework\TestCase;
use Spotahome\Core\Advert\ValueObject\AdvertSort;

class AdvertSortTest extends TestCase
{

    /**
     * @test
     */
    public function itCreates(): void
    {
        $sortBy = new AdvertSort('title');

        self::assertSame('title', $sortBy->getValue());
    }

    /**
     * @test
     */
    public function itThrowsFromInvalidValue(): void
    {
        self::expectException(\InvalidArgumentException::class);
        new AdvertSort('foo');
    }
}
