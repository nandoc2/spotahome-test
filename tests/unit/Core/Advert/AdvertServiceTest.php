<?php
declare(strict_types=1);

namespace Spotahome\Tests\Core\Advert;

use PHPUnit\Framework\TestCase;
use Prophecy\Prophecy\ObjectProphecy;
use Spotahome\Core\Advert\AdvertDbRepositoryInterface;
use Spotahome\Core\Advert\AdvertRemoteRepositoryInterface;
use Spotahome\Core\Advert\AdvertService;
use Spotahome\Core\Advert\ValueObject\AdvertSort;

class AdvertServiceTest extends TestCase
{
    /** @var AdvertService */
    private $advertService;

    /** @var AdvertDbRepositoryInterface|ObjectProphecy */
    private $dbRepository;

    /** @var AdvertRemoteRepositoryInterface|ObjectProphecy */
    private $remoteRepository;

    public function setUp(): void
    {
        $this->dbRepository = $this->prophesize(AdvertDbRepositoryInterface::class);
        $this->remoteRepository = $this->prophesize(AdvertRemoteRepositoryInterface::class);

        $this->advertService = new AdvertService(
            $this->dbRepository->reveal(),
            $this->remoteRepository->reveal()
        );
    }

    /**
     * @test
     */
    public function itImportsAdverts(): void
    {
        $this->remoteRepository
            ->fetchAll()
            ->shouldBeCalled()
            ->willReturn([]);

        $this->dbRepository
            ->bulkInsert([])
            ->shouldBeCalled();

        $this->advertService->importAdverts();
    }

    /**
     * @test
     */
    public function itGetsAllSortedBy(): void
    {
        $sortBy = new AdvertSort('id');

        $this->dbRepository
            ->getAllSortedBy($sortBy)
            ->shouldBeCalled();

        $this->advertService->getAllSortedBy($sortBy);
    }
}
