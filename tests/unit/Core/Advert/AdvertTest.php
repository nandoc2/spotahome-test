<?php
declare(strict_types=1);

namespace Spotahome\Tests\Core\Advert;

use PHPUnit\Framework\TestCase;
use Spotahome\Core\Advert\Advert;

class AdvertTest extends TestCase
{
    /**
     * @test
     */
    public function itCreates(): void
    {
        $advert = new Advert(
            '1',
            '2',
            'http://foo.com',
            'Madrid',
            'http://foo.com/img'
        );

        self::assertSame('1', $advert->getId());
        self::assertSame('2', $advert->getTitle());
        self::assertSame('http://foo.com', $advert->getLink());
        self::assertSame('Madrid', $advert->getCity());
        self::assertSame('http://foo.com/img', $advert->getMainImage());
    }
}
